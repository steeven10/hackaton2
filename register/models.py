from django.db import models

# Create your models here.
from django.utils.timezone import now


class Bank(models.Model):
    date = models.DateField()
    description = models.CharField(max_length=50)
    deposits = models.FloatField(max_length=50)
    withdrawls = models.FloatField(max_length=50)
    balance = models.FloatField(max_length=50)


# Model for the csv file
class InputCsv(models.Model):
    uploadedFile = models.FileField(upload_to="files/")


# Model for the xlsx file
class OrdersFile(models.Model):
    name = models.CharField(max_length=50)
    uploadedFile = models.FileField(upload_to="files/")


class Order(models.Model):
    region = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    item_type = models.CharField(max_length=50)
    sales_channel = models.CharField(max_length=250)
    order_priority = models.CharField(max_length=10)
    order_date = models.DateField()
    order_id = models.IntegerField(default=0)
    ship_date = models.DateField(default=now)
    units_sold = models.IntegerField(default=0)
    unit_price = models.FloatField(default=0)
    unit_cost = models.FloatField(default=0)
    total_revenue = models.FloatField(default=0)
    total_cost = models.FloatField(default=0)
    total_profit = models.FloatField(default=0)
