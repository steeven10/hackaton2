from django.contrib import admin
from register.models import Bank, Order, OrdersFile

# Register your models here.
admin.site.register(Bank)
admin.site.register(Order)
admin.site.register(OrdersFile)

